import React from 'react';
import { Row , Col, Card} from 'react-bootstrap';

export default function Highlights () {
    return (
        <Row>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Occaecat eu irure velit aliquip aliqua enim in reprehenderit enim duis. Adipisicing ea tempor occaecat aliqua proident enim. Eu Lorem quis minim nulla nostrud do. Ipsum incididunt esse aute elit cupidatat. Amet nisi laborum eiusmod anim mollit est Lorem quis Lorem mollit. Elit eu anim deserunt eiusmod velit quis exercitation occaecat sunt eu ex labore qui sunt.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Occaecat eu irure velit aliquip aliqua enim in reprehenderit enim duis. Adipisicing ea tempor occaecat aliqua proident enim. Eu Lorem quis minim nulla nostrud do. Ipsum incididunt esse aute elit cupidatat. Amet nisi laborum eiusmod anim mollit est Lorem quis Lorem mollit. Elit eu anim deserunt eiusmod velit quis exercitation occaecat sunt eu ex labore qui sunt.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                        <h2>Be part of Our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Occaecat eu irure velit aliquip aliqua enim in reprehenderit enim duis. Adipisicing ea tempor occaecat aliqua proident enim. Eu Lorem quis minim nulla nostrud do. Ipsum incididunt esse aute elit cupidatat. Amet nisi laborum eiusmod anim mollit est Lorem quis Lorem mollit. Elit eu anim deserunt eiusmod velit quis exercitation occaecat sunt eu ex labore qui sunt.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}