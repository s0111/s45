// import React from 'react';
import React, { useState } from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard ({courseProp}) {
    const { name, description, price } = courseProp;

    const [count, setCount] = useState(30);

	console.log(count);


	const enroll = () => {
		setCount(count - 1);
		console.log("Enrollees: " + count);
        if (count === 0) {
            alert("Course already fully booked, there is no more slot!");
        }
	}

    return (
      
                <Card className="m-3">
                    <Card.Body>
                        <Card.Title> { name } </Card.Title>
                        <Card.Subtitle>Description: </Card.Subtitle>
                        <Card.Text> { description } </Card.Text>
                        <Card.Subtitle>Price: </Card.Subtitle>
                        <Card.Text>Php { price }</Card.Text>
                        <Card.Subtitle>Enrollees: </Card.Subtitle>
                        <Card.Text>slot: {count}</Card.Text>
                        <Button variant = "primary" onClick={enroll}>Enroll now!</Button>
                    </Card.Body>
                </Card>
    )
}

CourseCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	courseProp: PropTypes.shape({
		//Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
