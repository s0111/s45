import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';

//React Js does not like rendering two adjacent elements. Instead the adjacent elements must be wrapped by a parent element or React Fragments (<>...</> or <React.Fragment>...</React.Fragment>)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  
    <App />
 
);

